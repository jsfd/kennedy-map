/// <reference path="../../../../../typings/jquery/jquery.d.ts" />
import {Component} from 'angular2/core';

@Component({
  selector: 'sd-map',
  templateUrl: 'app/+home/components/map.component.html',
  styleUrls: ['app/+home/components/map.component.css'],
})

export class MapComponent {}
