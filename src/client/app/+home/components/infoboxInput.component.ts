import {Component} from 'angular2/core';
import {UserInfoService} from '../../shared/services/userInfo.service';

@Component({
	selector: 'sd-infobox-input',
	template: `
		<div class="infobox-input">
			<p class="input-title">每月收入</p>
			<span class="dollar-wrapper number">$<input (keyup.enter)="onSubmit(salary.value)" type="number" name="salary" [(ngModel)]="mySalary" #salary value={{mySalary}}></span>
		</div>
	`,
	styleUrls: ['app/+home/components/salaryInput.component.css']
})

export class InfoboxInputComponent {
  mySalary: number;
  $salary: jQuery;

  constructor(private _userInfoService: UserInfoService) {
	  this.mySalary = _userInfoService.user.salary;
  }

  ngOnInit() {
		var $salary = jQuery('input[name="salary"]');
		var resize = function() {
				$salary.css('width', $salary.val().length * 25);
	  	};
		$salary.keyup(resize);
	 }

  onSubmit() {
	  this._userInfoService.updateSalary(this.mySalary);
  }
}
