/// <reference path="../../../../../typings/jquery/jquery.d.ts" />
import {Component, HostListener} from 'angular2/core';
import {CORE_DIRECTIVES} from 'angular2/common';
import {MapComponent} from './map.component';
import {PinComponent} from './pin.component';
import {InfoBoxComponent} from './infobox.component';
import {PopUpComponent} from './popup.component';
import {UserInfoService} from '../../shared/services/userInfo.service';


declare var jQuery: JQueryStatic;

@Component({
  selector: 'sd-home',
  templateUrl: 'app/+home/components/home.component.html',
  styleUrls: ['app/+home/components/home.component.css'],
  directives: [CORE_DIRECTIVES, MapComponent, PinComponent, InfoBoxComponent, PopUpComponent]
})

export class HomeComponent {

  $window: any;
  $document: any;
  $sceneContainer: any;
  $svgpin: any;

  isMouseDown : boolean = false;
  isDamping : boolean = false;
  oriPosX : number = 0;
  posDiff: number = 0;
  trackingX: number = 0;
  pinLevitate : number = 0;
  isLevitated : boolean = false;
  isLevitating : boolean = false;
  levitateDuration : number = 500;
  windowWidth: number;
  side: string = 'up';
  selectedEstate = {
      id: '',
      name: '',
      ppst: ''
  };
  user: any;

  constructor(private _userInfoService: UserInfoService) {
    this.user = _userInfoService.getUser();
  }

  @HostListener('mousemove', ['$event'])
  onMousemove(e: MouseEvent) {
    this.$window = jQuery(window);
    this.$document = jQuery(document);
    this.$sceneContainer = jQuery('#scene-container');
    this.$svgpin = jQuery('svg.pin');

    this.windowWidth = this.$window.width();
    if (this.isMouseDown) {
      this.posDiff = this.oriPosX - e.pageX;
      if (e.pageY > this.$window.height() / 2) {
        this.trackingX = this.trackingX + this.posDiff * 0.2;
        this.side = 'down';
      } else {
        this.trackingX = this.trackingX - this.posDiff * 0.2;
        this.side = 'up';
      }
    } else if (!this.isDamping) {
      this.posDiff = this.oriPosX - e.pageX;
      this.trackingX = this.trackingX + this.posDiff*0.01;
    }
    this.oriPosX = e.pageX;
  }

  @HostListener('mousedown', ['$event'])
  onMousedown(e: MouseEvent) {
    this.isMouseDown = true;
    this.isDamping = false;
    this.posDiff = 0;
    this.oriPosX = e.pageX;
  }

  @HostListener('mouseup')
  onMouseup() {
      this.isMouseDown = false;
      this.isDamping = true;
      this.inertia(this.posDiff);
  }

  inertia = function(speed: number) {
    if( Math.abs(speed) > 0.4 && this.isDamping === true ) {
      speed = speed * 0.99;
      if (this.side === 'down') {
        this.trackingX = this.trackingX + speed * 0.02;
      } else {
        this.trackingX = this.trackingX - speed * 0.02;
      }
      setTimeout(() => {
      this.inertia(speed);
      }, 10);
    } else {
      this.isDamping = false;
    }
  };
}
