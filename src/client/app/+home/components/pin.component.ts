/// <reference path="../../../../../typings/jquery/jquery.d.ts" />
import {Component, EventEmitter} from 'angular2/core';
import {UserInfoService} from '../../shared/services/userInfo.service';
import {EstateService} from '../../shared/services/estates.service';
import {Estate} from '../../shared/model/estate';

declare var jQuery: JQueryStatic;

@Component({
  selector: 'sd-pin',
  template: `
  <div class="kennedy-section">

  </div>
  <div class="ground-section">
    <div class="pin" [ngClass]="{kennedy: estate.isKennedy, affortable: !estate.isKennedy}" *ngFor="#estate of estates" 
    style="transform: translateX({{estate.coorX}}vw) translateY({{estate.coorY}}vw) translateZ(-8px) rotateX(-90deg) rotateY({{trackingX}}deg)">
      <span>
        <svg [attr.data-estate]="estate.id" (click)="onMousedown($event,estate)" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        width="86.6px" height="140.2px" viewBox="0 0 86.6 140.2" xml:space="preserve">
          <g>
            <path fill="#D6DCC2" d="M75.08,37.278c0,20.728-37.54,61.86-37.54,61.86S0,58.006,0,37.278c0-20.72,16.807-37.54,37.54-37.54
    S75.08,16.558,75.08,37.278z"/>
          </g>
          
        </svg>
      </span>
     </div>
   </div>
  `,
  styleUrls: ['app/+home/components/pin.component.css'],
  inputs: ['trackingX'],
  outputs: ['selectedEstate']
})

export class PinComponent {
  trackingX = '';
  pinID: number;
  estates: any;
  user = {};

  public estate: Estate;

  constructor(private _userInfoService: UserInfoService,
              private _estateService: EstateService) {
                this.user = _userInfoService.getUser();
              }

  ngOnInit() {

    this.estates = [];

    this._estateService.getEstates().subscribe(
      data => {
        this.estates = data;
      },
      error => console.error(error)
    );

    this._userInfoService.changedSalaryEvent.subscribe(salary => {
      // for (var j = 0; j < this.estates.length; ++j) {
      //   this.estates[j].isKennedy = (salary < this.estates[j].ppst) ? true : false;
      // }
      this.stressTestCal(salary);
      setTimeout(pinAnimation, 1000);
      function pinAnimation() {
        var intervalId = setInterval(function() {
          if (jQuery('.kennedy-section').css('transform').split(',')[14] <= 50.1) {
            clearInterval(intervalId);
            jQuery('.ground-section .kennedy').each(function() {
              jQuery('.kennedy-section').append(jQuery(this).detach());
            });
            jQuery('.kennedy-section .affortable').each(function() {
              jQuery('.ground-section').append(jQuery(this).detach());
            });
          }
        }, 100);
      }
    });

  }

  stressTestCal(salary) {
    for (var j = 0; j < this.estates.length; ++j) {
      this.estates[j].isKennedy = salary * 0.6 < this.estates[j].ppst * this.estates[j].sizes[0] * this._userInfoService.user.cachedFactor;
      console.log(this._userInfoService.user.cachedFactor);
    }
  }

  selectedEstate = new EventEmitter<{ id: number, name: string, ppst: number }>();

  onMousedown(e: MouseEvent, estate: {}) {
    this.selectedEstate.emit(estate);
    this._userInfoService.updateEstate(estate);

    var that = this;
    var d = new Date();
    var n = d.getFullYear();

    jQuery('.counting').each(function() {
      var type: string = jQuery(this).data('type');
      if (that._userInfoService.user.previousSelectedEstate[type] === 'undefined') {
        that._userInfoService.user.previousSelectedEstate[type] = 0;
      }

      if (type === 'occupation_dates') {
        jQuery(this)
        .prop('number', (n - that._userInfoService.user.previousSelectedEstate[type]))
        .animateNumber({
        number: (n - that._userInfoService.user.selectedEstate[type]),
          easing: 'easeOutQuad'
        }, 800);
      } else {
        jQuery(this)
        .prop('number', that._userInfoService.user.previousSelectedEstate[type])
        .animateNumber({
          number: that._userInfoService.user.selectedEstate[type],
          easing: 'easeOutQuad'
        }, 800);
      }
    })

    // Bar animation
    jQuery.each(jQuery('div[class*="infobox-bar-style"].container'), function() {
      var category = jQuery(this).data('category');
      var unit = jQuery(this).data('unit');
      var mode = jQuery(this).data('mode');

      var post_level: number = 1;
      var pre_level: number = jQuery('[data-category= "'+category+'"] span.fill').length;

      if (mode === 'custom') {
        if (category === 'no_of_block') {
          var rangeCategory = [1, 2, 5, 10, 15, 20, 30, 40, 50, 100];
        } else if (category === 'occupation_dates') {
          var rangeCategory = [1, 2, 5, 10, 15, 20, 30, 40, 50, 100];
        }

        jQuery.each(rangeCategory, function(index, value) {

          if (unit === 'year') {
            if (value < (n - that._userInfoService.user.selectedEstate[category])) {
              post_level++;
            } else {
              return false;
            }
          } else {
            if (value < that._userInfoService.user.selectedEstate[category]) {
              post_level++;
            } else {
              return false;
            }
          }
        });
      } else {
      post_level = Math.ceil(Math.sqrt(that._userInfoService.user.selectedEstate[category]));
      if (post_level>100) {
          post_level = 100;
        }
      }

      var step = Math.abs(post_level - pre_level);
      if (pre_level < post_level) {
        var addIntervalTimeout = setInterval(addInterval, 800 / step);
      } else if (pre_level > post_level) {
        var removeIntervalTimeout = setInterval(removeInterval, 800 / step);
      } //when pre = post, don't do anything

      function addInterval() {
        jQuery('[data-category="' + category + '"] span[data-interval="' + pre_level + '"]').addClass('fill');
        pre_level++;
        if (pre_level === post_level) {
          clearInterval(addIntervalTimeout);
        }
      }
      function removeInterval() {
        jQuery('[data-category="' + category + '"] span[data-interval="' + pre_level + '"]').removeClass('fill');
        pre_level--;
        if (pre_level === post_level) {
          clearInterval(removeIntervalTimeout);
        }
      }
    });
  }
}
