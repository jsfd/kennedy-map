/// <reference path="../../../../../typings/jquery/jquery.d.ts" />
import {Component} from 'angular2/core';
import {UserInfoService} from '../../shared/services/userInfo.service';
import {InfoboxInputComponent} from '../../+home/components/infoboxInput.component';

@Component({
	selector: 'sd-infobox',
	templateUrl: 'app/+home/components/infobox.component.html',
	styleUrls: ['app/+home/components/infobox.component.css'],
	inputs: ['selectedEstate','trackingX'],
	directives: [InfoboxInputComponent]
})

export class InfoBoxComponent {
  selectedEstate: any;
  salaryMarkerPos: number;
  salaryUpper: 40000;
  salaryLower: 0;
  interestRate: 0.00215;
  totalPrice: number;
  mortgageYearsNum: 0;
  mortgageYearsStr: '';
  trackingX: number;
  user: any;
  infoboxStyle1Limit: 100;
  infoboxStyle1LimitArray: any;
  infoboxStyle2Limit: 10;
  infoboxStyle2LimitArray: any;
  infoboxYearLimit: 5;
  infoboxYearLimitArray: any;
  userPreviousMontlyPaid: number = 0;
  knobTimeout: number;

  constructor(private _userInfoService: UserInfoService) {
		this.user = _userInfoService.getUser();
		this.infoboxStyle1LimitArray=[];
		for (var i: number = 1; i <=this.infoboxStyle1Limit ; i++) {
			this.infoboxStyle1LimitArray.push(i);
		}
		this.infoboxStyle2LimitArray=[];
		for (var i: number = 1; i <=this.infoboxStyle2Limit ; i++) {
			this.infoboxStyle2LimitArray.push(i);
		}
		this.infoboxYearLimitArray=[];
		for (var i: number = 1; i <=this.infoboxYearLimit ; i++) {
			this.infoboxYearLimitArray.push(i);
		}
  }
  onChangeSalary(salary) {
		this.moveSalaryMarker(salary);
		this.calculateYear(this.user.selectedEstate, this.user.selectedSize, salary, this.user.mortgagePercent);
  }
  onChangeEstate(estate) {
		this.calculateYear(estate, this.user.salary, this.user.mortgagePercent);
		this.calculatePrice(estate);
  }
  onChangePercent(percent) {
		console.log('onChangePercent called');
		this.calculateYear(this.user.selectedEstate, this.user.selectedSize, this.user.salary, percent);
  }
  onChangeYear(year) {
		this.mortgageYearsNum = year;
		this.mortgageYearsStr = this.mortgageYearsNum.toString();
		this.calculatePercent(year);
		jQuery('input.percentage')
			.val(this.user.mortgagePercent)
			.trigger('change');
		this.updateMonthlyPay();
  }
  onChangeSize(size) {
		this._userInfoService.updateSize(size);
		this.calculatePrice(this.user.selectedEstate);
		this.calculateYear(this.user.selectedEstate, size, this.user.salary, this.user.mortgagePercent);
  }
  updateMonthlyPay() {
		jQuery('.monthly-paid')
			.prop('number', this.userPreviousMontlyPaid)
			.animateNumber({
				number: this.user.mortgagePercent * this.user.salary * 0.01,
				easing: 'easeOutQuad'
			}, 800);
		this.userPreviousMontlyPaid = this.user.mortgagePercent * this.user.salary * 0.01;
  }
  moveSalaryMarker(salary) {
		this.salaryMarkerPos = (salary / (this.salaryUpper - this.salaryLower)) * 100 ;
  }
  calculatePrice(estate) {
		this.totalPrice = estate.ppst * this.user.selectedSize;
  }
  calculateYear(estate, size, salary, percent) {
		// ( Math.log(1-((interest/12)*price)/monthlyPay) / Math.log(1+interest/12) ) / -12

		this.mortgageYearsNum = Math.round((Math.log(1 - ((this.interestRate / 12) * estate.ppst * size) / (salary * percent * 0.01)) / Math.log(1 + this.interestRate / 12)) / -12);
		this.mortgageYearsStr = Number.isNaN(this.mortgageYearsNum) ? '∞' : this.mortgageYearsNum.toString();
		jQuery('.infobox-year-slider').nstSlider('set_position', this.mortgageYearsNum);
  }
  calculatePercent(year) {
		if (!jQuery.isEmptyObject(this.user.selectedEstate)) {
			this._userInfoService.updatePercent(
				Math.round((
					(this.user.selectedEstate.ppst * this.user.selectedSize * (this.interestRate / 12)) / (1 - Math.pow((1 + (this.interestRate / 12)), -12 * year))
				) * 100 / this.user.salary)
			);
		}
  }
  calculateCachedFactor(year) {
		this._userInfoService.updateCached((this.interestRate / 12) / (1 - Math.pow((1 + (this.interestRate / 12)), -12 * year)));
  }
  ngOnInit() {
	  var that = this;
		jQuery('input.percentage')
			.knob({
				'change': (function(value) {
					this.user.mortgagePercent = Math.round(value);
					this._userInfoService.updatePercent(Math.round(value));
					window.clearTimeout(this.knobTimeout);
					this.knobTimeout = window.setTimeout(this.updateMonthlyPay.bind(this), 50);
				}).bind(this)
			});
		jQuery('input.percentage').val(this.user.mortgagePercent).trigger('change');
		jQuery('.infobox-year-slider').nstSlider({
        'left_grip_selector': '.slider-handle',
	    'value_changed_callback': (function(cause, leftValue, rightValue) {
				if (cause !== 'set_position') {
					this.onChangeYear(leftValue);
				}
			}).bind(this)
		});

		this.moveSalaryMarker(this.user.salary);
		this.calculateCachedFactor(25);
		this._userInfoService.changedSalaryEvent.subscribe(this.onChangeSalary.bind(this));
		this._userInfoService.changedEstateEvent.subscribe(this.onChangeEstate.bind(this));
		this._userInfoService.changedPercentEvent.subscribe(this.onChangePercent.bind(this));
		$('.infobox-container').css('height', $('.tab-1').height());
		jQuery('.tab-selector').on('click', 'li', function() {
			console.log(jQuery(this).data('tab'));
			that._userInfoService.changeTab(jQuery(this).data('tab'));
			$('.infobox-container').css('height', $('.tab-' + jQuery(this).data('tab')).height());
		});
  }
}
