import {Component} from 'angular2/core';
import {UserInfoService} from '../../shared/services/userInfo.service';

@Component({
	selector: 'sd-popup',
	templateUrl: 'app/+home/components/popup.component.html',
	styleUrls: ['app/+home/components/popup.component.css']
})

export class PopUpComponent {
	user: any;
	constructor(private _userInfoService: UserInfoService) {
		this.user = _userInfoService.getUser();
	}
	hidePopUp() {
		this._userInfoService.hideResult();
	}
}
