export class Estate {
    constructor(
		public id: number ,
		public name: string,
		public address: string,
		public ppst: number,
	 	public coorX: number,
		public coorY: number,
        public coorZ: number,
        public developer: string,
        public sizes: [],
        public uility_rate: number,
        public year_of_publish: number
    ) {}
}
