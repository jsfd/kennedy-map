import {Injectable} from 'angular2/core';
import {EventEmitter} from 'angular2/core';

@Injectable()
export class UserInfoService {

	changedSalaryEvent: EventEmitter = new EventEmitter();
  changedEstateEvent: EventEmitter = new EventEmitter();
  changedPercentEvent: EventEmitter = new EventEmitter();
  changedSizeEvent: EventEmitter = new EventEmitter();

  user = {
    salary: 12000,
    isEnterSalary: false,
    isViewResult: false,
    isPoor: false,
    isRich: false,
    mortgagePercent: 50,
    selectedEstate: {},
    selectedSize: 0,
    previousSelectedEstate: {},
    cachedFactor: 0,
    customBoxTabNo: 1
  };

	getUser() {
    return this.user;
  }

  updateSalary(mySalary: number) {
    this.user.salary = mySalary;
    this.user.isEnterSalary = true;
    //this.user.isViewResult = true;
    this.changedSalaryEvent.emit(mySalary);
    this.user.customBoxTabNo = 0;
  }

  updatePercent(percent: number) {
    this.user.mortgagePercent = percent;
    this.changedPercentEvent.emit(percent);
  }

  updateEstate(estate: {}) {
    this.user.previousSelectedEstate = this.user.selectedEstate;
    this.user.selectedEstate = estate;
    this.user.selectedSize = estate.sizes[0];
    this.changedEstateEvent.emit(estate);
    this.user.customBoxTabNo = 0;
    $('.infobox-container').css('height', 0);
  }

  updateSize(size) {
    this.user.selectedSize = size;
    this.changedSizeEvent.emit(size);
  }

  updateCached(value) {
    this.user.cachedFactor = value;
  }

  log() {
		console.log(this.user);
  }

  hideResult() {
    this.user.isViewResult = false;
  }

  changeTab(value:number) {
    console.log(value);
    this.user.customBoxTabNo = value;
  }
}
