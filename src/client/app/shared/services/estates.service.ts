import { Injectable } from 'angular2/core';
import { Http } from 'angular2/http';
import 'rxjs/add/operator/map';
import { Observable }     from 'rxjs/Observable';
import { Estate }     from '../model/estate';

@Injectable()
export class EstateService {

  constructor(private _http: Http) {}

  getEstates(): Observable<Estate> {
    return this._http.get('dist/dev/app/data/estates.json')
      .map(response => response.json());
  }
}
