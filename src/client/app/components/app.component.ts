import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES, RouteConfig} from 'angular2/router';
import {ToolbarComponent} from '../shared/components/toolbar.component';
//import {NameListService} from '../shared/index';
import {HomeComponent} from '../+home/index';

@Component({
  selector: 'sd-app',
  templateUrl: 'app/components/app.component.html',
  directives: [ROUTER_DIRECTIVES, ToolbarComponent]
})
@RouteConfig([
  {
    path: '/',
    name: 'Home',
    component: HomeComponent
  }
])
export class AppComponent {}
